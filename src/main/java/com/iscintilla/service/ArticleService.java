package com.iscintilla.service;

import com.iscintilla.domain.model.Article;
import com.iscintilla.domain.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Created by leaftoe on 10/18/17.
 */
@Service
public class ArticleService {

    private final ArticleRepository articleRepository;

    @Autowired
    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public Flux<Article> findAllArticles() {
        return articleRepository.findAll();
    }

    public Mono<Article> findArticleById(String id) {
        return articleRepository.findByIdOrSlug(id, id);
    }

    public Mono<Article> createArticle(Article article) {
        return articleRepository.save(article);
    }

    public Mono<Article> updateArticle(String id, Article article) {
        return articleRepository.findById(id)
                .flatMap(existingArticle -> {
                    existingArticle.setSlug(article.getSlug());
                    existingArticle.setText(article.getText());
                    existingArticle.setTitle(article.getTitle());

                    return articleRepository.save(existingArticle);
                });
    }

    public Mono<Void> deleteArticle(Article article) {
        return articleRepository.delete(article);
    }
}