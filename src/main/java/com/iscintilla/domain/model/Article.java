package com.iscintilla.domain.model;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by leaftoe on 10/18/17.
 */
@Document
public class Article extends AbstractDocument {

    private String slug;
    private String title;
    private String text;

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
