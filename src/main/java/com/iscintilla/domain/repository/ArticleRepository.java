package com.iscintilla.domain.repository;

import com.iscintilla.domain.model.Article;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

/**
 * Created by leaftoe on 10/18/17.
 */
public interface ArticleRepository extends ReactiveCrudRepository<Article, String> {

    Mono<Article> findByIdOrSlug(String id, String slug);
}